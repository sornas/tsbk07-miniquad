use glam::Mat4;
use glam::{Vec3, Vec4};
use miniquad::*;

#[rustfmt::skip]
const VERTICES: [Vertex; 18] = [
    Vertex { pos: Vec3::new(-0.5, -0.5, 0.0), color: Vec4::new(1.0, 0.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new(-0.5,  0.5, 0.0), color: Vec4::new(1.0, 0.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new( 0.5,  0.5, 0.0), color: Vec4::new(1.0, 0.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new(-0.5, -0.5, 0.0), color: Vec4::new(0.0, 1.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new( 0.5,  0.5, 0.0), color: Vec4::new(0.0, 1.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new( 0.5, -0.5, 0.0), color: Vec4::new(0.0, 1.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new(-0.5, -0.5, 0.0), color: Vec4::new(0.0, 0.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new( 0.5, -0.5, 0.0), color: Vec4::new(0.0, 0.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new( 0.0,  0.0, 0.5), color: Vec4::new(0.0, 0.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new( 0.5, -0.5, 0.0), color: Vec4::new(1.0, 1.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new( 0.5,  0.5, 0.0), color: Vec4::new(1.0, 1.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new( 0.0,  0.0, 0.5), color: Vec4::new(1.0, 1.0, 0.0, 1.0) },
    Vertex { pos: Vec3::new( 0.5,  0.5, 0.0), color: Vec4::new(1.0, 0.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new(-0.5,  0.5, 0.0), color: Vec4::new(1.0, 0.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new( 0.0,  0.0, 0.5), color: Vec4::new(1.0, 0.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new(-0.5,  0.5, 0.0), color: Vec4::new(0.0, 1.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new(-0.5, -0.5, 0.0), color: Vec4::new(0.0, 1.0, 1.0, 1.0) },
    Vertex { pos: Vec3::new( 0.0,  0.0, 0.5), color: Vec4::new(0.0, 1.0, 1.0, 1.0) },
];

#[repr(C)]
struct Vertex {
    pos: Vec3,
    color: Vec4,
}

struct Stage {
    pipeline: Pipeline,
    bindings: Bindings,
    ctx: Box<dyn RenderingBackend>,
}

impl Stage {
    pub fn new() -> Stage {
        let mut ctx: Box<dyn RenderingBackend> = window::new_rendering_backend();

        let vertex_buffer = ctx.new_buffer(
            BufferType::VertexBuffer,
            BufferUsage::Immutable,
            BufferSource::slice(&VERTICES),
        );

        let indices: Vec<_> = (0..VERTICES.len() as u32).collect();
        let index_buffer = ctx.new_buffer(
            BufferType::IndexBuffer,
            BufferUsage::Immutable,
            BufferSource::slice(&indices),
        );

        let bindings = Bindings {
            vertex_buffers: vec![vertex_buffer],
            index_buffer,
            images: vec![],
        };

        let shader = ctx
            .new_shader(
                ShaderSource::Glsl {
                    vertex: shader::VERTEX,
                    fragment: shader::FRAGMENT,
                },
                shader::meta(),
            )
            .unwrap();

        let pipeline = ctx.new_pipeline(
            &[BufferLayout::default()],
            &[
                VertexAttribute::new("in_pos", VertexFormat::Float3),
                VertexAttribute::new("in_color", VertexFormat::Float4),
            ],
            shader,
            PipelineParams {
                depth_test: Comparison::Less,
                depth_write: true,
                ..Default::default()
            },
        );

        Stage {
            pipeline,
            bindings,
            ctx,
        }
    }
}

impl EventHandler for Stage {
    fn update(&mut self) {}

    fn draw(&mut self) {
        self.ctx.begin_default_pass(Default::default());

        let t = date::now();

        let rot_matrix = Mat4::from_rotation_x((t.sin() * 2.0) as f32)
            * Mat4::from_rotation_y((t.sin() * 2.0) as f32);

        self.ctx.apply_pipeline(&self.pipeline);
        self.ctx.apply_bindings(&self.bindings);
        self.ctx
            .apply_uniforms(UniformsSource::table(&shader::Uniforms { rot_matrix }));
        self.ctx.draw(0, VERTICES.len() as i32, 1);
        self.ctx.end_render_pass();

        self.ctx.commit_frame();
    }
}

fn main() {
    let conf = conf::Conf::default();
    miniquad::start(conf, move || Box::new(Stage::new()));
}

mod shader {
    use glam::Mat4;
    use miniquad::*;

    pub const VERTEX: &str = r#"
    #version 150

    in  vec3 in_pos;
    in  vec4 in_color;
    out vec4 v_color;

    uniform mat4 rot_matrix;

    void main(void) {
        gl_Position = rot_matrix * vec4(in_pos, 1.0);
        v_color = in_color;
    }
    "#;

    pub const FRAGMENT: &str = r#"
    #version 150

    in vec4 v_color;

    void main(void) {
        gl_FragColor = v_color;
    }
    "#;

    pub fn meta() -> ShaderMeta {
        ShaderMeta {
            images: vec![],
            uniforms: UniformBlockLayout {
                uniforms: vec![UniformDesc::new("rot_matrix", UniformType::Mat4)],
            },
        }
    }

    #[repr(C)]
    pub struct Uniforms {
        pub rot_matrix: Mat4,
    }
}
