use glam::{Mat4, Vec3};
use miniquad::*;
use std::f32::consts::PI;
use std::path::Path;

type Context = Box<miniquad::Context>;

fn open_model(ctx: &mut Context, path: impl AsRef<Path>) -> (Bindings, usize) {
    let model = tobj::load_obj(
        path.as_ref(),
        &tobj::LoadOptions {
            single_index: true,
            ..tobj::GPU_LOAD_OPTIONS
        },
    )
    .unwrap();

    let mesh = &model.0[0].mesh;
    let mut vertices = vec![];

    for i in 0..mesh.positions.len() / 3 {
        vertices.extend_from_slice(&[
            mesh.positions[i * 3 + 0],
            mesh.positions[i * 3 + 1],
            mesh.positions[i * 3 + 2],
        ]);

        if !mesh.normals.is_empty() {
            vertices.extend_from_slice(&[
                mesh.normals[i * 3 + 0],
                mesh.normals[i * 3 + 1],
                mesh.normals[i * 3 + 2],
            ]);
        } else {
            vertices.extend_from_slice(&[0.0, 1.0, 0.0])
        }

        if !mesh.texcoords.is_empty() {
            vertices.extend_from_slice(&[mesh.texcoords[i * 3 + 0], mesh.texcoords[i * 3 + 1]]);
        } else {
            vertices.extend_from_slice(&[0.0, 0.0])
        }
    }

    let vertex_buffer = ctx.new_buffer(
        BufferType::VertexBuffer,
        BufferUsage::Immutable,
        BufferSource::slice(&vertices),
    );

    let index_buffer = ctx.new_buffer(
        BufferType::IndexBuffer,
        BufferUsage::Immutable,
        BufferSource::slice(&mesh.indices),
    );

    (
        Bindings {
            vertex_buffers: vec![vertex_buffer],
            index_buffer,
            images: vec![],
        },
        mesh.indices.len(),
    )
}

struct Stage {
    ctx: Context,
    t_0: f64,
    pipeline: Pipeline,
    bunny: (Bindings, usize),
    teddy: (Bindings, usize),
}

impl Stage {
    fn new() -> Self {
        let mut ctx = window::new_rendering_backend();

        let bunny = open_model(&mut ctx, "bunny.obj");
        let teddy = open_model(&mut ctx, "teddy.obj");

        let shader = ctx
            .new_shader(
                ShaderSource::Glsl {
                    vertex: shader::VERTEX,
                    fragment: shader::FRAGMENT,
                },
                shader::meta(),
            )
            .unwrap();

        let pipeline = ctx.new_pipeline(
            &[BufferLayout::default()],
            &[
                VertexAttribute::new("inPosition", VertexFormat::Float3),
                VertexAttribute::new("inNormal", VertexFormat::Float3),
                VertexAttribute::new("inTexCoord", VertexFormat::Float2),
            ],
            shader,
            PipelineParams {
                depth_test: Comparison::Less,
                depth_write: true,
                ..Default::default()
            },
        );

        Self {
            ctx,
            t_0: date::now(),
            pipeline,
            bunny,
            teddy,
        }
    }
}

impl EventHandler for Stage {
    fn update(&mut self) {}
    fn draw(&mut self) {
        self.ctx
            .begin_default_pass(PassAction::clear_color(0.0, 0.2, 0.6, 1.0));
        let t = (date::now() - self.t_0) as f32;

        let projection = Mat4::perspective_rh_gl(PI / 2.5, 1.0, 1.0, 30.0);
        let world_to_view = Mat4::look_at_rh(
            Vec3::new(t.sin() * 3.0, 2.0, t.cos() * 3.0),
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.5, 2.0, 0.0),
        );

        self.ctx.apply_pipeline(&self.pipeline);

        let model_to_world = Mat4::IDENTITY;
        self.ctx.apply_bindings(&self.bunny.0);
        self.ctx
            .apply_uniforms(UniformsSource::table(&shader::Uniforms {
                projection,
                model_to_world,
                world_to_view,
            }));
        self.ctx.draw(0, self.bunny.1 as i32, 1);

        let model_to_world = Mat4::from_translation(Vec3::new(0.0, 0.0, 1.5))
            * Mat4::from_rotation_y(t * 2.0)
            * Mat4::from_scale(Vec3::new(0.7, 0.7, 0.7));
        self.ctx.apply_bindings(&self.teddy.0);
        self.ctx
            .apply_uniforms(UniformsSource::table(&shader::Uniforms {
                projection,
                model_to_world,
                world_to_view,
            }));
        self.ctx.draw(0, self.teddy.1 as i32, 1);
    }
}

fn main() {
    let conf = conf::Conf::default();
    miniquad::start(conf, move || Box::new(Stage::new()));
}

mod shader {
    use glam::Mat4;
    use miniquad::*;

    pub const VERTEX: &str = r#"
    #version 150

    in  vec3 inPosition;
    in  vec3 inNormal;
    out vec4 vert_Color;

    in  vec2 inTexCoord;
    out vec2 vTexCoord;

    uniform mat4 projectionMatrix;
    uniform mat4 worldToView;
    uniform mat4 modelToWorld;

    const vec3 light = vec3(2, 2, 0);

    void main(void) {
        mat3 normalMatrix = mat3(modelToWorld);
        vec3 transformedNormal = normalMatrix * inNormal;
        float gr = dot(normalize(light - inPosition), normalize(transformedNormal));
        vert_Color = vec4(gr+0.1, gr+0.1, gr+0.1, 1.0);

        gl_Position = projectionMatrix * worldToView * modelToWorld * vec4(inPosition, 1.0);
        vTexCoord = inTexCoord;
    }
    "#;

    pub const FRAGMENT: &str = r#"
    #version 150

    in  vec4 vert_Color;
    in  vec2 vTexCoord;
    // uniform sampler2D texUnit;

    void main(void) {
        gl_FragColor = vert_Color;
    }
    "#;

    pub fn meta() -> ShaderMeta {
        ShaderMeta {
            images: vec![],
            uniforms: UniformBlockLayout {
                uniforms: vec![
                    UniformDesc::new("projectionMatrix", UniformType::Mat4),
                    UniformDesc::new("worldToView", UniformType::Mat4),
                    UniformDesc::new("modelToWorld", UniformType::Mat4),
                ],
            },
        }
    }

    #[repr(C)]
    pub struct Uniforms {
        pub projection: Mat4,
        pub world_to_view: Mat4,
        pub model_to_world: Mat4,
    }
}
