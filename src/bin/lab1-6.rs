use glam::*;
use miniquad::*;

struct Stage {
    ctx: Box<dyn RenderingBackend>,

    pipeline: Pipeline,
    bindings: Bindings,
    num_vertices: i32,
}

impl Stage {
    pub fn new() -> Stage {
        let mut ctx: Box<dyn RenderingBackend> = window::new_rendering_backend();

        let model = tobj::load_obj(
            "bunny.obj",
            &tobj::LoadOptions {
                single_index: true,
                ..tobj::GPU_LOAD_OPTIONS
            },
        )
        .unwrap();
        let mesh = &model.0[0].mesh;
        let mut vertices = vec![];
        let mut normals = vec![];
        let mut indices = vec![];

        for i in 0..mesh.positions.len() / 3 {
            vertices.extend_from_slice(&[
                mesh.positions[i * 3 + 0],
                mesh.positions[i * 3 + 1],
                mesh.positions[i * 3 + 2],
            ]);
        }

        for i in 0..mesh.normals.len() / 3 {
            normals.extend_from_slice(&[
                mesh.normals[i * 3 + 0],
                mesh.normals[i * 3 + 1],
                mesh.normals[i * 3 + 2],
            ]);
        }

        for i in 0..mesh.indices.len() / 3 {
            indices.extend_from_slice(&[
                mesh.indices[i * 3 + 0],
                mesh.indices[i * 3 + 1],
                mesh.indices[i * 3 + 2],
            ]);
        }

        dbg!(vertices.len());
        dbg!(normals.len());
        dbg!(indices.len());

        let vertex_buffer = ctx.new_buffer(
            BufferType::VertexBuffer,
            BufferUsage::Immutable,
            BufferSource::slice(&vertices),
        );

        let index_buffer = ctx.new_buffer(
            BufferType::IndexBuffer,
            BufferUsage::Immutable,
            BufferSource::slice(&indices),
        );

        let bindings = Bindings {
            vertex_buffers: vec![vertex_buffer /*, normal_buffer*/],
            index_buffer,
            images: vec![],
        };

        let shader = ctx
            .new_shader(
                ShaderSource::Glsl {
                    vertex: shader::VERTEX,
                    fragment: shader::FRAGMENT,
                },
                shader::meta(),
            )
            .unwrap();

        let pipeline = ctx.new_pipeline(
            &[BufferLayout::default()],
            &[
                VertexAttribute::new("in_pos", VertexFormat::Float3),
                // VertexAttribute::new("in_normal", VertexFormat::Float3),
            ],
            shader,
            PipelineParams {
                depth_test: Comparison::Less,
                depth_write: true,
                ..Default::default()
            },
        );

        Stage {
            ctx,
            pipeline,
            bindings,
            num_vertices: indices.len() as i32,
        }
    }
}

impl EventHandler for Stage {
    fn update(&mut self) {}

    fn draw(&mut self) {
        self.ctx.begin_default_pass(Default::default());

        let t = date::now();

        let rot_matrix = Mat4::from_rotation_x((t.sin() * 2.0) as f32)
            * Mat4::from_rotation_y((t.sin() * 2.0) as f32);

        self.ctx.apply_pipeline(&self.pipeline);
        self.ctx.apply_bindings(&self.bindings);
        self.ctx
            .apply_uniforms(UniformsSource::table(&shader::Uniforms { rot_matrix }));
        self.ctx.draw(0, self.num_vertices, 1);
        self.ctx.end_render_pass();

        self.ctx.commit_frame();
    }
}

fn main() {
    let conf = conf::Conf::default();
    miniquad::start(conf, move || Box::new(Stage::new()));
}

mod shader {
    use glam::Mat4;
    use miniquad::*;

    pub const VERTEX: &str = r#"
    #version 150

    in  vec3 in_pos;
    out vec4 v_color;

    uniform mat4 rot_matrix;

    void main(void) {
        gl_Position = rot_matrix * vec4(in_pos, 1.0);
        v_color = abs(rot_matrix * vec4(in_pos, 1.0) * 2.0);
    }
    "#;

    pub const FRAGMENT: &str = r#"
    #version 150

    in vec4 v_color;

    void main(void) {
        gl_FragColor = v_color;
    }
    "#;

    pub fn meta() -> ShaderMeta {
        ShaderMeta {
            images: vec![],
            uniforms: UniformBlockLayout {
                uniforms: vec![UniformDesc::new("rot_matrix", UniformType::Mat4)],
            },
        }
    }

    #[repr(C)]
    pub struct Uniforms {
        pub rot_matrix: Mat4,
    }
}
