use std::collections::HashMap;
use std::f32::consts::PI;
use std::path::Path;

use glam::*;
use miniquad::*;
use stb_image::image::Image;

type RenderContext = Box<dyn RenderingBackend>;

struct Heightmap {
    data: Vec<f32>,
    width: usize,
    height: usize,
}

impl Heightmap {
    fn height_at(&self, x: f32, z: f32) -> f32 {
        let xi = x.trunc();
        let zi = z.trunc();
        let xd = x - xi;
        let zd = z - zi;

        let p1;
        let p2;
        let p3;

        if xd + zd < 1.0 {
            p1 = Vec3::new(xi + 0.0, 0.0, zi + 0.0);
            p2 = Vec3::new(xi + 1.0, 0.0, zi + 0.0);
            p3 = Vec3::new(xi + 0.0, 0.0, zi + 1.0);
        } else {
            p1 = Vec3::new(xi + 1.0, 0.0, zi + 1.0);
            p2 = Vec3::new(xi + 1.0, 0.0, zi + 0.0);
            p3 = Vec3::new(xi + 0.0, 0.0, zi + 1.0);
        }

        let p1xi = p1.x as usize;
        let p1zi = p1.z as usize;

        let p2xi = p2.x as usize;
        let p2zi = p2.z as usize;

        let p3xi = p3.x as usize;
        let p3zi = p3.z as usize;

        let quot = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);

        let w1 = ((p2.z - p3.z) * (x - p3.x) + (p3.x - p2.x) * (z - p3.z)) / quot;
        let w2 = ((p3.z - p1.z) * (x - p3.x) + (p1.x - p3.x) * (z - p3.z)) / quot;
        let w3 = 1.0 - w1 - w2;

        let y1 = self.data[(p1xi % self.width) + (p1zi % self.height) * (self.width)];
        let y2 = self.data[(p2xi % self.width) + (p2zi % self.height) * (self.width)];
        let y3 = self.data[(p3xi % self.width) + (p3zi % self.height) * (self.width)];

        (w1 * y1 + w2 * y2 + w3 * y3) / (w1 + w2 + w3)
    }
}

fn open_image(path: impl AsRef<Path>) -> Image<u8> {
    let bytes = match stb_image::image::load(path.as_ref()) {
        stb_image::image::LoadResult::Error(e) => panic!("{}", e),
        stb_image::image::LoadResult::ImageU8(bytes) => bytes,
        stb_image::image::LoadResult::ImageF32(_floats) => todo!(),
    };
    bytes
}

fn open_obj(path: impl AsRef<Path>) -> (Vec<f32>, Vec<u32>) {
    let model = tobj::load_obj(
        path.as_ref(),
        &tobj::LoadOptions {
            single_index: true,
            ..tobj::GPU_LOAD_OPTIONS
        },
    )
    .unwrap();

    let mesh = &model.0[0].mesh;
    let mut vertices = vec![];

    for i in 0..mesh.positions.len() / 3 {
        vertices.extend_from_slice(&[
            mesh.positions[i * 3 + 0],
            mesh.positions[i * 3 + 1],
            mesh.positions[i * 3 + 2],
        ]);

        if !mesh.normals.is_empty() {
            vertices.extend_from_slice(&[
                mesh.normals[i * 3 + 0],
                mesh.normals[i * 3 + 1],
                mesh.normals[i * 3 + 2],
            ]);
        } else {
            todo!()
        }

        if !mesh.texcoords.is_empty() {
            vertices.extend_from_slice(&[mesh.texcoords[i * 3 + 0], mesh.texcoords[i * 3 + 1]]);
        } else {
            vertices.extend_from_slice(&[0.0, 0.0])
        }
    }

    (vertices, mesh.indices.clone())
}

fn open_heightmap(path: impl AsRef<Path>) -> (Vec<f32>, Vec<u32>, Heightmap) {
    let heightmap_image = open_image(path.as_ref());
    let w = heightmap_image.width;
    let h = heightmap_image.height;
    let vcount = w * h;
    let tricount = (w - 1) * (h - 1) * 2;
    let mut positions = vec![Vec3::ZERO; vcount];
    let mut normals = vec![Vec3::Y; vcount];
    let mut tcoords = vec![Vec2::ZERO; vcount];
    let mut indices = vec![0; tricount * 3];
    let pixels = &heightmap_image.data;
    for x in 0..w {
        for z in 0..h {
            let c = pixels[x + z * w * 3];
            positions[x + z * w] = Vec3 {
                x: x as f32,
                y: c as f32 / 10.0,
                z: z as f32,
            };
            tcoords[x + z * w] = Vec2 {
                x: x as f32,
                y: z as f32,
            }
        }
    }
    for x in 0..w {
        for z in 0..h {
            let p0 = positions[x + z * w];

            let mut tp1 = Vec2::new(x as f32, z as f32);
            let mut tp2 = Vec2::new(x as f32, z as f32);
            let mut tp3 = Vec2::new(x as f32, z as f32);
            let mut tp4 = Vec2::new(x as f32, z as f32);
            let mut tp5 = Vec2::new(x as f32, z as f32);
            let mut tp6 = Vec2::new(x as f32, z as f32);

            if x + 1 == w {
                tp1.x = 0.0;
                tp6.x = 0.0;
            }
            if x == 0 {
                tp3.x = w as f32;
                tp4.x = w as f32;
            }

            if z + 1 == h {
                tp5.y = 0.0;
                tp6.y = 0.0;
            }
            if z == 0 {
                tp2.y = h as f32;
                tp3.y = h as f32;
            }

            let mut p1 = positions[(tp1.x as usize + 1) + (tp1.y as usize + 0) * w];
            let mut p2 = positions[(tp2.x as usize + 0) + (tp2.y as usize - 1) * w];
            let mut p3 = positions[(tp3.x as usize - 1) + (tp3.y as usize - 1) * w];
            let mut p4 = positions[(tp4.x as usize - 1) + (tp4.y as usize + 0) * w];
            let mut p5 = positions[(tp5.x as usize + 0) + (tp5.y as usize + 1) * w];
            let mut p6 = positions[(tp6.x as usize + 1) + (tp6.y as usize + 1) * w];

            if x + 1 == w {
                p1.x += w as f32;
                p6.x += w as f32;
            }

            if x == 0 {
                p3.x -= w as f32;
                p4.x -= w as f32;
            }

            if z + 1 == h {
                p5.z += h as f32;
                p6.z += h as f32;
            }

            if z == 0 {
                p2.z -= h as f32;
                p3.z -= h as f32;
            }

            let n1 = (p1 - p0).cross(p2 - p0);
            let n2 = (p2 - p0).cross(p3 - p0);
            let n3 = (p3 - p0).cross(p4 - p0);
            let n4 = (p4 - p0).cross(p5 - p0);
            let n5 = (p5 - p0).cross(p6 - p0);
            let n6 = (p6 - p0).cross(p1 - p0);

            normals[x + z * w] = [n1 * 2.0, n2, n3, n4 * 2.0, n5, n6]
                .iter()
                .sum::<Vec3>()
                .normalize();
        }
    }

    for x in 0..(w - 1) {
        for z in 0..(h - 1) {
            indices[(x + z * (w - 1)) * 6 + 0] = ((x + 0) + (z + 0) * w) as u32;
            indices[(x + z * (w - 1)) * 6 + 1] = ((x + 0) + (z + 1) * w) as u32;
            indices[(x + z * (w - 1)) * 6 + 2] = ((x + 1) + (z + 0) * w) as u32;
            indices[(x + z * (w - 1)) * 6 + 3] = ((x + 1) + (z + 0) * w) as u32;
            indices[(x + z * (w - 1)) * 6 + 4] = ((x + 0) + (z + 1) * w) as u32;
            indices[(x + z * (w - 1)) * 6 + 5] = ((x + 1) + (z + 1) * w) as u32;
        }
    }

    let mut vertices = vec![];
    for i in 0..positions.len() {
        vertices.extend_from_slice(&[positions[i].x, positions[i].y, positions[i].z]);
        vertices.extend_from_slice(&[normals[i].x, normals[i].y, normals[i].z]);
        vertices.extend_from_slice(&[tcoords[i].x, tcoords[i].y]);
    }

    (
        vertices,
        indices,
        Heightmap {
            data: positions.iter().map(|v| v.y).collect(),
            width: w,
            height: h,
        },
    )
}

struct Model {
    bindings: Bindings,
    num_vertices: i32,
}

impl Model {
    fn open(ctx: &mut RenderContext, vertices: Vec<f32>, indices: Vec<u32>) -> Self {
        let vertex_buffer = ctx.new_buffer(
            BufferType::VertexBuffer,
            BufferUsage::Immutable,
            BufferSource::slice(&vertices),
        );

        let index_buffer = ctx.new_buffer(
            BufferType::IndexBuffer,
            BufferUsage::Immutable,
            BufferSource::slice(&indices),
        );

        let grass = open_image("grass.tga");
        let tex1 = ctx.new_texture_from_data_and_format(
            &grass.data,
            TextureParams {
                kind: TextureKind::Texture2D,
                width: grass.width as u32,
                height: grass.height as u32,
                format: TextureFormat::RGBA8,
                wrap: TextureWrap::Repeat,
                min_filter: FilterMode::Linear,
                mag_filter: FilterMode::Linear,
                mipmap_filter: MipmapFilterMode::Linear,
                allocate_mipmaps: true,
            },
        );
        ctx.texture_generate_mipmaps(tex1);

        let dirt = open_image("dirt.tga");
        let tex2 = ctx.new_texture_from_data_and_format(
            &dirt.data,
            TextureParams {
                kind: TextureKind::Texture2D,
                width: dirt.width as u32,
                height: dirt.height as u32,
                format: TextureFormat::RGBA8,
                wrap: TextureWrap::Repeat,
                min_filter: FilterMode::Linear,
                mag_filter: FilterMode::Linear,
                mipmap_filter: MipmapFilterMode::Linear,
                allocate_mipmaps: true,
            },
        );
        ctx.texture_generate_mipmaps(tex2);

        let bindings = Bindings {
            vertex_buffers: vec![vertex_buffer],
            index_buffer,
            images: vec![tex1, tex2],
        };

        Self {
            bindings,
            num_vertices: indices.len() as i32,
        }
    }

    fn open_sphere(ctx: &mut RenderContext) -> Self {
        let (v, i) = open_obj("groundsphere.obj");
        Self::open(ctx, v, i)
    }

    fn open_terrain(ctx: &mut RenderContext) -> (Self, Heightmap) {
        let (v, i, heightmap) = open_heightmap("fft-terrain.tga");
        (Self::open(ctx, v, i), heightmap)
    }
}

struct Stage {
    ctx: RenderContext,
    #[cfg(feature = "egui")]
    egui_mq: egui_miniquad::EguiMq,

    prev_t: f64,
    keys_down: HashMap<KeyCode, bool>,
    camera_p: Vec3,
    camera_lookh: f32,

    pipeline: Pipeline,
    terrain: Model,
    sphere: Model,
    heightmap: Heightmap,
}

impl Stage {
    pub fn new() -> Stage {
        let mut ctx: RenderContext = window::new_rendering_backend();

        let shader = ctx
            .new_shader(
                ShaderSource::Glsl {
                    vertex: shader::VERTEX,
                    fragment: shader::FRAGMENT,
                },
                shader::meta(),
            )
            .unwrap();

        let pipeline = ctx.new_pipeline(
            &[BufferLayout::default()],
            &[
                VertexAttribute::new("inPosition", VertexFormat::Float3),
                VertexAttribute::new("inNormal", VertexFormat::Float3),
                VertexAttribute::new("inTexCoord", VertexFormat::Float2),
            ],
            shader,
            PipelineParams {
                depth_test: Comparison::Less,
                depth_write: true,
                ..Default::default()
            },
        );

        let (terrain, heightmap) = Model::open_terrain(&mut ctx);
        let sphere = Model::open_sphere(&mut ctx);

        Stage {
            #[cfg(feature = "egui")]
            egui_mq: egui_miniquad::EguiMq::new(&mut *ctx),
            ctx,
            prev_t: date::now(),
            keys_down: HashMap::new(),
            camera_p: Vec3::ZERO,
            camera_lookh: 1.0,
            pipeline,
            terrain,
            sphere,
            heightmap,
        }
    }
}

impl EventHandler for Stage {
    fn update(&mut self) {}

    fn mouse_motion_event(&mut self, _x: f32, _y: f32) {
        #[cfg(feature = "egui")]
        self.egui_mq.mouse_motion_event(_x, _y);
    }

    fn mouse_wheel_event(&mut self, _x: f32, _y: f32) {
        #[cfg(feature = "egui")]
        self.egui_mq.mouse_wheel_event(_x, _y);
    }

    fn mouse_button_down_event(&mut self, _button: MouseButton, _x: f32, _y: f32) {
        #[cfg(feature = "egui")]
        self.egui_mq.mouse_button_down_event(_button, _x, _y);
    }

    fn mouse_button_up_event(&mut self, _button: MouseButton, _x: f32, _y: f32) {
        #[cfg(feature = "egui")]
        self.egui_mq.mouse_button_up_event(_button, _x, _y);
    }

    fn char_event(&mut self, _character: char, _keymods: KeyMods, _repeat: bool) {
        #[cfg(feature = "egui")]
        self.egui_mq.char_event(_character);
    }

    fn raw_mouse_motion(&mut self, dx: f32, _dy: f32) {
        self.camera_lookh -= dx / 500.0;
    }

    fn key_down_event(&mut self, keycode: KeyCode, _keymods: KeyMods, _repeat: bool) {
        *self.keys_down.entry(keycode).or_insert(true) = true;
        #[cfg(feature = "egui")]
        self.egui_mq.key_down_event(keycode, _keymods);
    }

    fn key_up_event(&mut self, keycode: KeyCode, _keymods: KeyMods) {
        *self.keys_down.entry(keycode).or_insert(false) = false;
        #[cfg(feature = "egui")]
        self.egui_mq.key_up_event(keycode, _keymods);
    }

    fn draw(&mut self) {
        self.ctx
            .begin_default_pass(PassAction::clear_color(0.5, 0.5, 0.5, 1.0));

        let t = date::now();
        let delta = (t - self.prev_t) as f32;
        self.prev_t = t;

        const MOVE_SPEED: f32 = 10.0;

        if self.keys_down.get(&KeyCode::W) == Some(&true) {
            self.camera_p.x -= delta * self.camera_lookh.cos() * MOVE_SPEED;
            self.camera_p.z += delta * self.camera_lookh.sin() * MOVE_SPEED;
        }
        if self.keys_down.get(&KeyCode::S) == Some(&true) {
            self.camera_p.x += delta * self.camera_lookh.cos() * MOVE_SPEED;
            self.camera_p.z -= delta * self.camera_lookh.sin() * MOVE_SPEED;
        }
        if self.keys_down.get(&KeyCode::A) == Some(&true) {
            self.camera_p.x -= delta * (-self.camera_lookh).sin() * MOVE_SPEED;
            self.camera_p.z += delta * (-self.camera_lookh).cos() * MOVE_SPEED;
        }
        if self.keys_down.get(&KeyCode::D) == Some(&true) {
            self.camera_p.x += delta * (-self.camera_lookh).sin() * MOVE_SPEED;
            self.camera_p.z -= delta * (-self.camera_lookh).cos() * MOVE_SPEED;
        }
        self.camera_p.y = self.heightmap.height_at(self.camera_p.x, self.camera_p.z) + 0.5;

        let proj_matrix = Mat4::perspective_rh_gl(PI / 2.0, 1.0, 0.1, 200.0);
        let cam_matrix = Mat4::look_at_rh(
            self.camera_p,
            self.camera_p + Vec3::new(-self.camera_lookh.cos(), 0.0, -(-self.camera_lookh).sin()),
            Vec3::Y,
        );
        self.ctx.apply_pipeline(&self.pipeline);

        self.ctx.apply_bindings(&self.terrain.bindings);
        self.ctx
            .apply_uniforms(UniformsSource::table(&shader::Uniforms {
                proj_matrix,
                mdl_matrix: cam_matrix,
                world_to_view: cam_matrix,
            }));
        self.ctx.draw(0, self.terrain.num_vertices, 1);

        let mut ball_p = self.camera_p + Vec3::new(2.0 * t.sin() as f32, 0.0, 2.0 * t.cos() as f32);
        ball_p.y = self.heightmap.height_at(ball_p.x, ball_p.z);
        self.ctx.apply_bindings(&self.sphere.bindings);
        self.ctx
            .apply_uniforms(UniformsSource::table(&shader::Uniforms {
                proj_matrix,
                mdl_matrix: cam_matrix
                    * Mat4::from_translation(ball_p)
                    * Mat4::from_scale(Vec3::new(0.2, 0.2, 0.2)),
                world_to_view: cam_matrix,
            }));
        self.ctx.draw(0, self.sphere.num_vertices, 1);

        self.ctx.end_render_pass();

        #[cfg(feature = "egui")]
        {
            self.egui_mq.run(&mut *self.ctx, |_mq_ctx, egui_ctx| {
                egui::Window::new("debug window").show(egui_ctx, |ui| {
                    if ui.button("Quit").clicked() {
                        std::process::exit(0);
                    }
                });
            });
            self.egui_mq.draw(&mut *self.ctx);
        }

        self.ctx.commit_frame();
    }
}

fn main() {
    let conf = conf::Conf::default();
    miniquad::start(conf, move || Box::new(Stage::new()));
}

mod shader {
    use glam::Mat4;
    use miniquad::*;

    pub const VERTEX: &str = r#"
    #version 150

    in  vec3 inPosition;
    in  vec3 inNormal;
    out vec3 vPosition;
    out vec3 vNormal;

    in  vec2 inTexCoord;
    out vec2 vtexCoord;

    out float height;

    uniform mat4 proj_matrix;
    uniform mat4 mdl_matrix;

    void main(void) {
        mat3 normalMatrix1 = mat3(mdl_matrix);
        vtexCoord = inTexCoord;
        gl_Position = proj_matrix * mdl_matrix * vec4(inPosition, 1.0);

        vPosition = vec3(mdl_matrix * vec4(inPosition, 1.0));
        vNormal = normalMatrix1 * inNormal;
        height = inPosition.y;
    }
    "#;

    pub const FRAGMENT: &str = r#"
    #version 150

    in vec2 vtexCoord;
    uniform sampler2D tex1;
    uniform sampler2D tex2;

    uniform mat4 world_to_view;

    in vec3 vPosition;
    in vec3 vNormal;
    in float height;

    vec3 lightDir = vec3(0.0, 0.5, 1.0);

    float blend_h = 3.0;
    float dirt_h = 12.0;

    float fog_start = 2.0;
    float fog_end = 10.0;

    vec4 blend(vec4 c1, vec4 c2, float b) {
        return b * c1 + (1.0 - b) * c2;
    }

    void main(void) {
        if (height == 0.0) {
            gl_FragColor = vec4(0.0, 0.3, 1.0, 1.0);
        } else {
            vec3 n = normalize(vNormal);
            vec3 s = normalize(mat3(world_to_view) * lightDir);
            float heightBlend = min(max((height - blend_h )/(dirt_h - blend_h), 0.0), 1.0);
            gl_FragColor = (0.1 + 0.9 * max(0.0, dot(n, s))) * blend(texture(tex2, vtexCoord), texture(tex1, vtexCoord), heightBlend);
        }
        float dist = sqrt(sqrt(dot(vPosition, vPosition)));
        float distBlend = min(max((dist - fog_start)/(fog_end - fog_start), 0.0), 1.0);
        gl_FragColor = blend(
            vec4(0.5, 0.5, 0.5, 1.0),
            gl_FragColor,
            distBlend
        );
    }
    "#;

    pub fn meta() -> ShaderMeta {
        ShaderMeta {
            images: vec!["tex1".to_string(), "tex2".to_string()],
            uniforms: UniformBlockLayout {
                uniforms: vec![
                    UniformDesc::new("proj_matrix", UniformType::Mat4),
                    UniformDesc::new("mdl_matrix", UniformType::Mat4),
                    UniformDesc::new("world_to_view", UniformType::Mat4),
                ],
            },
        }
    }

    #[repr(C)]
    pub struct Uniforms {
        pub proj_matrix: Mat4,
        pub mdl_matrix: Mat4,
        pub world_to_view: Mat4,
    }
}
